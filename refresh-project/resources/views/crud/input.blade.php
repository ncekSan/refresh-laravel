<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>AdminLTE 3 | Starter</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="lte/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="lte/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  @include('admin/header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('admin/sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">List Data</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="col-md-6">
            <div class="card">
              <div class="card-header" >
                <h3 class="card-title">Formulir Deteksi Dini (COVID-19)</h3>
                <br/>
                <p>Deteksi dini Covid-19 bagi civitas akademika UNSIKA</p>
                <p style="color:red">*Wajib</p>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputEmail">Alamat Email*</label>
                    <input type="email" class="form-control" id="inputEmail" placeholder="Email Anda">
                  </div>
                  <div class="form-group">
                    <label for="inputNama">Nama*</label>
                    <input type="text" class="form-control" id="inputNama" placeholder="Jawaban Anda">
                  </div>
                  <div class="form-group">
                    <label for="inputNPM">NPM/NIS*</label>
                    <input type="text" class="form-control" id="inputNPM" placeholder="Jawaban Anda">
                  </div>
                  <div class="form-group">
                    <label for="inputUsia">Usia*</label>
                    <input type="text" class="form-control" id="inputUsia" placeholder="Jawaban Anda">
                  </div>
                  <div class="form-group">
                    <p><b>JenisKelamin*</b></p>
                    <input type="radio" id="pria" name="gender" value="pria">
                    <label for="pria">Pria</label>
                    <br/>
                    <input type="radio" id="wanita" name="gender" value="wanita">
                    <label for="wanita">Wanita</label>
                  </div>
                  <div class="form-group">
                    <label for="inputTelp">Telp*</label>
                    <input type="text" class="form-control" id="inputTelp" placeholder="Jawaban Anda">
                  </div>
                  <div class="form-group">
                    <label for="inputAlamatSekarang">Alamat Saat Ini*</label>
                    <input type="text" class="form-control" id="inputAlamatSekarang" placeholder="Jawaban Anda">
                  </div>
                  <div class="form-group">
                    <label for="inputAlamatAsal">Alamat Asal*</label>
                    <input type="text" class="form-control" id="inputAlamatAsal" placeholder="Jawaban Anda">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="lte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="lte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="lte/dist/js/adminlte.min.js"></script>
</body>
</html>
