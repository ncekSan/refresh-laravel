@extends('starter')

@section('show')

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Email</th>
                    <th>Name</th>
                    <th>NPM/NIS</th>
                    <th>Age</th>
                    <th>Gender</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Origins</th>
                    <th>Location</th>
                    <th colspan="2"></th>
                </tr>
            </thead>
            <tbody>

                @foreach ($data as $d)
                <tr>
                    <td>{{$d->email}}</td>
                    <td>{{$d->nama}}</td>
                    <td>{{$d->npm/nis}}</td>
                    <td>{{$d->usia}}</td>
                    <td>{{$d->jenis_kelamin}}</td>
                    <td>{{$d->telp}}</td>
                    <td>{{$d->alamat_sekarang}}</td>
                    <td>{{$d->alamat_asal}}</td>
                    <td>{{$d->posisi_sekarang}}</td>
                    <td><button class="btn btn-warning">Edit</button></td>
                    <td><button class="btn btn-danger">Delete</button></td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</div>

@endsection